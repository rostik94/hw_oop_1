const btn = document.getElementById('button');
const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');
const width = 400;
const height = 400;

canvas.width = width;
canvas.height = height;

class Point {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }

  getRandomProperties(maxWidth, maxHeight) {
    this.x = Math.round(Math.random() * maxWidth);
    this.y = Math.round(Math.random() * maxHeight);
  }
}

class Rect extends Point {
  constructor(x, y, width, height) {
    super(x, y);
    this.width = width;
    this.height = height;
  }

  getRandomProperties(maxWidth, maxHeight) {
    super.getRandomProperties(maxWidth, maxHeight);

    this.width = Math.round(Math.random() * (maxWidth - this.x));
    this.height = Math.round(Math.random() * (maxHeight - this.y));
  }

  getRectIntersection(rect) {
    const intersRect = {};

    if (this.x >= rect.x) {
      intersRect.x = this.x;

      if (
        this.width < rect.width &&
        this.x + this.width < rect.x + rect.width
      ) {
        intersRect.width = this.width;
      } else {
        intersRect.width = rect.x + rect.width - intersRect.x;
      }
    } else if (this.x < rect.x) {
      intersRect.x = rect.x;

      if (
        rect.width < this.width &&
        rect.x + rect.width < this.x + this.width
      ) {
        intersRect.width = rect.width;
      } else {
        intersRect.width = this.x + this.width - intersRect.x;
      }
    }

    if (this.y >= rect.y) {
      intersRect.y = this.y;

      if (
        this.height < rect.height &&
        this.y + this.height < rect.y + rect.height
      ) {
        intersRect.height = this.height;
      } else {
        intersRect.height = rect.y + rect.height - intersRect.y;
      }
    } else if (this.y < rect.y) {
      intersRect.y = rect.y;

      if (
        rect.height < this.height &&
        rect.y + rect.height < this.y + this.height
      ) {
        intersRect.height = rect.height;
      } else {
        intersRect.height = this.y + this.height - intersRect.y;
      }
    }

    return new Rect(
      intersRect.x,
      intersRect.y,
      intersRect.width,
      intersRect.height
    );
  }

  isIntersection(rect) {
    if (
      rect.x <= this.x + this.width &&
      this.x <= rect.x + rect.width &&
      rect.y <= this.y + this.height &&
      this.y <= rect.y + rect.height
    ) {
      return true;
    }
  }
}

class Canvas {
  cleanCanvas() {
    ctx.clearRect(0, 0, width, height);
  }

  drawRect(rect, color) {
    ctx.beginPath();
    ctx.strokeStyle = color;
    ctx.strokeRect(rect.x, rect.y, rect.width, rect.height);
    ctx.closePath();
  }

  drawEllipse(ellipse, color) {
    ctx.beginPath();
    ctx.strokeStyle = color;
    ctx.ellipse(
      ellipse.x,
      ellipse.y,
      ellipse.width,
      ellipse.height,
      0,
      0,
      2 * Math.PI
    );
    ctx.stroke();
    ctx.fillStyle = 'rgba(0,0,0,0.4)';
    ctx.fill();
    ctx.closePath();
  }

  drawCircle(circle, color) {
    ctx.beginPath();
    ctx.strokeStyle = color;
    ctx.arc(circle.x, circle.y, 2, 0, 2 * Math.PI);
    ctx.stroke();
  }
}

class Ellipse extends Point {
  getParameters(innerRect) {
    this.x = innerRect.x + innerRect.width / 2;
    this.y = innerRect.y + innerRect.height / 2;
    this.width = innerRect.width / 2;
    this.height = innerRect.height / 2;
  }
}

class Circle extends Point {
  getParameters(innerRect) {
    const randomAngle = Math.random() * 2 * Math.PI;
    const randomX =
      (((Math.random() * innerRect.width) / 2) * innerRect.width) / 2;
    const randomY =
      (((Math.random() * innerRect.height) / 2) * innerRect.height) / 2;
    const dotX = Math.sqrt(randomX) * Math.cos(randomAngle);
    const dotY = Math.sqrt(randomY) * Math.sin(randomAngle);
    const pointX = dotX + innerRect.x + innerRect.width / 2;
    const pointY = dotY + innerRect.y + innerRect.height / 2;

    this.x = Math.round(pointX);
    this.y = Math.round(pointY);
  }
}

btn.addEventListener('click', () => {
  console.clear();

  const canvas1 = new Canvas();
  canvas1.cleanCanvas();

  const rect1 = new Rect();
  const rect2 = new Rect();

  rect1.getRandomProperties(width, height); // Юзати width, height
  rect2.getRandomProperties(width, height);

  canvas1.drawRect(rect1, 'blue');
  canvas1.drawRect(rect2, 'green');

  if (rect1.isIntersection(rect2)) {
    const innerRect = rect1.getRectIntersection(rect2);
    canvas1.drawRect(innerRect, 'orange');

    const innerEllipse = new Ellipse();
    innerEllipse.getParameters(innerRect);
    canvas1.drawEllipse(innerEllipse, 'red');

    const circleArray = [];

    for (let i = 0; i < 10; i++) {
      circleArray[i] = new Circle();
      circleArray[i].getParameters(innerRect);
      canvas1.drawCircle(circleArray[i], 'brown');
    }

    console.log(circleArray);
    console.log(innerEllipse);
    console.log(innerRect);
  }

  console.log(rect1);
  console.log(rect2);
});
